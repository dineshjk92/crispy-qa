const moment= require('moment') 
import AllFeedsPage from '../../../pages/AllFeedsPage';
import CommonPage from '../../../pages/CommonPage';

const allFeedsPage = new AllFeedsPage()
const commonPage = new CommonPage()

Given("I visit the application", () => {
  cy.visit("/");
  cy.title().should("eq", "Crispy Succotash");
});

Then("Verify logout link is displayed", () => {
  allFeedsPage.getLogoutLink().then((ele) => {
    expect(ele.text()).to.eq('Logout')
  });
});

Then("Verify logout link is not displayed", () => {
  cy.get('body').then((body) => {
    expect(body.find("a[href$='/logout/']").length === 0).to.be.true
  })
})

And('Logout of the application', () => {
  commonPage.logout()
})

And('Verify error {string} is displayed', (content) => {
  cy.contains(content, { setTimeout: 1000 }).should('be.visible')
})

And('I click on {string} feed page link', (pagename) => {
  switch (pagename) {
    case 'All':
      commonPage.gotoAllFeedsPage()
      break
    case 'My':
      commonPage.gotoMyFeedsPage()
      break
    case 'Bookmarked':
      commonPage.gotoBookmarkedFeedsPage()
      break
  }
})

And('I click on New Feed button', ()=> {
  commonPage.gotoNewFeedsPage()
})

Then('The feed {string} should be should be shown in the All page', (url)=> {
  cy.contains(url, {setTimeout: 1000}).should('be.visible')
})

And('I read the current date and time', ()=> {
  cy.wrap(moment.utc().format('MMM. DD, YYYY, h:mm a')).as('currentTimestamp')
})

And('I navigate back', ()=> {
  cy.go('back')
})