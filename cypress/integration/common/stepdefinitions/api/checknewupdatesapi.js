import CSRFTokenExtractor from "./csrftokenextractor"

When('I read the last checked time from the response', () => {
    cy.get('@response').then((response) => {
        expect(response.body).to.contain('Last checked')
        const regEx = new RegExp("(?<=Last\\schecked\\s*<\/dt>\\s*.*<dd>).*(?=\\s*<\/dd>)")
        var match = response.body.match(regEx)
        var lastCheckedTime = match[0]
        console.log(lastCheckedTime);
        cy.wrap(lastCheckedTime.replace('am', 'a.m.').replace('pm', 'p.m.')).as('lastCheckedTime')
    })
})

When('I read the update message from the response', () => {
    cy.get('@response').then((response) => {
        expect(response.body).to.contain('Last checked')
        const regEx = new RegExp("(?<=<div\\sclass=\"alert.*\"\\s*>).*(?=<\\s*\/div>)")
        var match = response.body.match(regEx)
        var updateMessage = match[0]
        console.log(updateMessage);
        cy.wrap(updateMessage).as('updateMessage')
    })
})

And('I make a call to update the feed', () => {
    cy.get('@feed_id').then((feed_id) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "GET",
                url: "/feeds/" + feed_id + "/update/",
                headers: {
                    "X-CSRFTOKEN": token,
                },
                "failOnStatusCode": false
            }).as('response')
        })
    })
})

Then('The response should contain the update message', () => {
    cy.get('@response').then((response) => {
        cy.get('@updateMessage').then((updateMessage) => {
            expect(response.body).to.contain(updateMessage)
        })
    })
})

And('Last checked time from the response should not be the same when the update alert is {string}', (foundNewUpdateAlert) => {
    cy.get('@response').then((response) => {
        cy.get('@updateMessage').then((updateMessage) => {
            cy.get('@lastCheckedTime').then((lastCheckedTime) => {
                if (updateMessage === foundNewUpdateAlert) {
                    expect(response.body).to.not.contain(lastCheckedTime)
                } else {
                    expect(response.body).to.contain(lastCheckedTime)
                }
            })
        })
    })
})

And('Entries count should be incremented when the update alert is {string}', (foundNewUpdateAlert) => {
    cy.get('@response').then((response) => {
        cy.get('@updateMessage').then((updateMessage) => {
            cy.get('@lastCheckedTime').then((lastCheckedTime) => {
                if (updateMessage === foundNewUpdateAlert) {
                    expect(response.body).to.not.contain(lastCheckedTime)
                } else {
                    expect(response.body).to.contain(lastCheckedTime)
                }
            })
        })
    })
})

Then('Entries count in the response for the {string} should not be the same when the update alert is {string}', (url, foundNewUpdateAlert) => {
    cy.get('@response').then((response) => {
        cy.get('@updateMessage').then((updateMessage) => {
            cy.get('@feed_entry_count').then((feed_entry_count) => {
                const regEx = new RegExp("(?<=<td>\\s*" + url + "\\s*<\/td>\\s*<td>).*(?=<\\s*\/td>)")
                var match = response.body.match(regEx)
                var newFeedEntryCount = match[0]
                console.log(newFeedEntryCount);
                if (updateMessage === foundNewUpdateAlert) {
                    expect(newFeedEntryCount).to.not.eq(feed_entry_count)
                } else {
                    expect(newFeedEntryCount).to.eq(feed_entry_count)
                }
            })
        })
    })
})