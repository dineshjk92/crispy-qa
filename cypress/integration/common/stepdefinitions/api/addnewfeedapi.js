//refer commonstepsapi.js from "../common/commonstepsapi.js"
//for more login related api functions

import CSRFTokenExtractor from "./csrftokenextractor"

And('I request add new feed page', () => {
    cy.get("@csrfToken").then((token) => {
        cy.request({
            method: "GET",
            url: "/feeds/new/",
            headers: {
                "X-CSRFTOKEN": token,
            },
        }).as('response')
    })
    cy.get('@response').then((response) => {
        CSRFTokenExtractor.storeCSRFToken(response)
    })
})

And('I add a new feed {string} via api', (url) => {
    cy.wrap(url).as('url')
    cy.get("@csrfToken").then((token) => {
        cy.request({
            method: "POST",
            url: "/feeds/new/",
            form: true,
            body: {
                feed_url: url
            },
            headers: {
                "X-CSRFTOKEN": token,
            },
            "failOnStatusCode": false
        }).as('response')
    })
})

And('I add the existing feed url', () => {
    cy.get("@feed_url").then((feed_url) => {
        cy.get("@csrfToken").then((token) => {
            cy.request({
                method: "POST",
                url: "/feeds/new/",
                form: true,
                body: {
                    feed_url: feed_url
                },
                headers: {
                    "X-CSRFTOKEN": token,
                },
            }).as('response')
        })
    })
})