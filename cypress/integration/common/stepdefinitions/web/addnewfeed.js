const addContext = require("mochawesome/addContext");
import LoginPage from '../../../pages/LoginPage';
import MainPage from "../../../pages/MainPage"
import NewFeedsPage from '../../../pages/NewFeedsPage';

const loginPage = new LoginPage()
const mainPage = new MainPage()
const newFeedsPage = new NewFeedsPage()


When('I click on the login link', () => {
    mainPage.clickLogin()
})

And("I login with credentials", (datatable) => {
  datatable.hashes().forEach((element) => {
    loginPage.login(element.username, element.password);
  })
})

And('I add a new feed {string}', (url) => {
  newFeedsPage.addNewFeedUrl(url)
})

And('I click on submit button', () => {
  newFeedsPage.clickSubmit()
})
