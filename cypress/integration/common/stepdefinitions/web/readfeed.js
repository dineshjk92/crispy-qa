const addContext = require("mochawesome/addContext");
import AllFeedsPage from '../../../pages/AllFeedsPage';
import CommonPage from '../../../pages/CommonPage';
import FeedsDetailListPage from '../../../pages/FeedsDetailListPage';
import FeedsDetailPage from '../../../pages/FeedsDetailPage';

const commonPage = new CommonPage()
const allFeedsPage = new AllFeedsPage()
const feedsDetailsListPage = new FeedsDetailListPage()
const feedsDetailPage = new FeedsDetailPage()


And('There are feeds available', ()=> {
  const expectHeaders = ['Title', 'Last updated', 'Feed URL', '# Entries']
  cy.get(feedsDetailsListPage.getTableHeader()).then((actualHeaders) => {
    expect(Object.values(actualHeaders)).to.contains.members(expectHeaders);
  })
})

And('I read the first feed\'s url & title', ()=> {
  feedsDetailsListPage.getFirstFeedsTitle().then((title) => {
    cy.wrap(title.text()).as('title')
  })
  feedsDetailsListPage.getFirstFeedsUrl().then((url) => {
    cy.wrap(url.text()).as('url')
  })
})

And('I click on the first feed from list', ()=> {
  feedsDetailsListPage.clickOnFirstFeed()
})

Then('Verify the feed title and url are displayed', ()=> {
  cy.get('@title').then((title) => {
    cy.contains(title).should('be.visible')
  })
  cy.get('@url').then((url) => {
    cy.contains(url).should('be.visible')
  })
})

And('I read the first feeds\' detail', ()=> {
  feedsDetailPage.getFirstFeedsTitle().then((title) => {
    cy.wrap(title.text()).as('title')
  })
})

And('I click on the first feed detail', ()=> {
  feedsDetailPage.clickOnFirstFeed()
})

Then('Verify the feeds detail is displayed', ()=> {
  cy.get('@title').then((title) => {
    cy.contains(title).should('be.visible')
  })
})