import BookmarkedFeedsPage from "../../../pages/BookmarkedFeedsPage"

const bookmarkedFeedsPage = new BookmarkedFeedsPage()

And('I click on bookmark button to {string}', (bookmark) => {
    if (bookmark === 'bookmark') {
        bookmarkedFeedsPage.getBookmarkButton().then((ele) => {
            if (ele.hasClass('glyphicon-heart-empty')) {
                bookmarkedFeedsPage.clickBookmarkButton()
            }
        })
    } else if (bookmark === 'removeBookmark') {
        bookmarkedFeedsPage.getBookmarkButton().then((ele) => {
            if (ele.hasClass('glyphicon-heart')) {
                bookmarkedFeedsPage.clickBookmarkButton()
            }
        })
    }
})


Then('Verify the feed title and url are displayed in the bookmarked page', () => {
    cy.get('@title').then((title) => {
        cy.contains(title).should('be.visible')
    })
    cy.get('@url').then((url) => {
        cy.contains(url).should('be.visible')
    })
})

Then('Verify the feed title and url are not displayed in the bookmarked page', () => {
    cy.get('@title').then((title) => {
        cy.contains(title).should('not.be.visible')
    })
    cy.get('@url').then((url) => {
        cy.contains(url).should('not.be.visible')
    })
})