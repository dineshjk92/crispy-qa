const addContext = require("mochawesome/addContext");
import LoginPage from "../../../pages/LoginPage";
import MainPage from "../../../pages/MainPage"
import SignupPage from '../../../pages/SignupPage';

const signupPage = new SignupPage()
const mainPage = new MainPage()
const loginPage = new LoginPage()

When('I click on the Signup button', () => {
  mainPage.clickSignup()
})

And("I login with the same credentials", () => {
  cy.get("@username").then((username) => {
    cy.get("@password").then((password) => {
      loginPage.login(username, password);    
    })
  })
})

And('I Signup with {string} and {string}', (username, password) => {
  const randomUsername = username + Math.floor((Math.random() * 10000) + 100)
  signupPage.signup(randomUsername, password);
  cy.wrap(randomUsername).as("username")
  cy.wrap(password).as("password")
})

And('I Signup with invalid {string} and {string}', (username, password) => {
  signupPage.signup(username, password);
})