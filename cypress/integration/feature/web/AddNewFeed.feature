@UI
Feature: Verify adding a new feed to the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed

    @smoke @test
    Scenario Outline: Add a valid Feed URL
        When I click on '<pagename>' feed page link
        And I click on New Feed button
        And I add a new feed '<url>'
        And I click on submit button
        Then The feed '<url>' should be should be shown in the All page
        Examples:
            | pagename   | url                                                                                 |
            | All        | http://www.nu.nl/rss/Algemeen                                                       |
            | My         | https://www.channelnewsasia.com/api/v1/rss-outbound-feed?_format=xml&category=10416 |
            | Bookmarked | https://www.channelnewsasi.com/api/v1/rss-outbound-feed?_format=xml&category=10416  |


    @smoke @test
    Scenario Outline: Add an invalid Feed URL
        When I click on '<pagename>' feed page link
        And I click on New Feed button
        And I add a new feed '<url>'
        And I click on submit button
        Then Verify error '<error>' is displayed
        Examples:
            | pagename | url                               | error                                   |
            | All      | http://www.nu.nl/rss/Algemeen     | Feed with this Feed URL already exists. |
            | All      | https://en.wikipedia.org/wiki/RSS | NoReverseMatch at /feeds/new/           |
            | All      | This is an invalid url            | Enter a valid URL.                      |

    @smoke @test
    Scenario Outline: Add an empty Feed URL
        When I click on '<pagename>' feed page link
        And I click on New Feed button
        And I click on submit button
        Then Verify error '<error>' is displayed
        Examples:
            | pagename | error                   |
            | All      | This field is required. |