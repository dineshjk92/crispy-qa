@UI
Feature: Verify the login functionality of the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link

    @smoke @test
    Scenario: Login with a valid credentials
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed
        And Logout of the application

    Scenario: Login with invalid credentials
        And I login with credentials
            | username | password |
            | autotest | pass     |
        Then Verify logout link is not displayed
        And Verify error "Please enter a correct username and password. Note that both fields may be case-sensitive." is displayed

    Scenario: Login with no username
        And I login with credentials
            | password |
            | pass     |
        Then Verify logout link is not displayed
        And Verify error "This field is required." is displayed

    Scenario: Login with no password
        And I login with credentials
            | username |
            | autotest |
        Then Verify logout link is not displayed
        And Verify error "This field is required." is displayed 

    Scenario: Login with captialized username
        And I login with credentials
            | username  | password |
            | AUTOTEST1 | pass1234 |
        Then Verify logout link is not displayed
        And Verify error "Please enter a correct username and password. Note that both fields may be case-sensitive." is displayed


