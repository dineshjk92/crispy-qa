@UI
Feature: Verify bookmark feeds function in the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed

    @smoke @test
    Scenario Outline: Bookmark a feed
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        And I click on bookmark button to "bookmark"
        And I click on 'Bookmarked' feed page link
        Then Verify the feed title and url are displayed in the bookmarked page
        Examples:
            | pagename |
            | All      |

    @smoke @test
    Scenario Outline: Remove bookmark from a feed
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        And I click on bookmark button to "removeBookmark"
        And I click on 'Bookmarked' feed page link
        Then Verify the feed title and url are not displayed in the bookmarked page
        Examples:
            | pagename |
            | All      |