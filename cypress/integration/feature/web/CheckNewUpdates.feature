@UI
Feature: Verify check new updates function in the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed

    @smoke @test
    Scenario Outline: Check for new updates alert message
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        And I click on Check Updates button
        Then The update alert message 'Nothing new yet...' or 'Found new updates. Enjoy!' should be displayed
        Examples:
            | pagename |
            | All      |

    @smoke @test
    Scenario Outline: Last updated time after checking new updates
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        And I read the current date and time
        And I click on Check Updates button
        Then The update alert message 'Nothing new yet...' or 'Found new updates. Enjoy!' should be displayed
        And Last checked time should not be the same when 'Found new updates. Enjoy!'
        Examples:
            | pagename |
            | All      |
    
    @smoke @test
    Scenario Outline: Number of entries for a feed before and after an update
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title 
        And I read the first feed's entries count
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        And I click on Check Updates button
        Then The update alert message 'Nothing new yet...' or 'Found new updates. Enjoy!' should be displayed
        When I click on '<pagename>' feed page link
        Then The feed's entries for the first feed should not match when 'Found new updates. Enjoy!'
        Examples:
            | pagename |
            | All      |