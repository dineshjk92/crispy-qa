@UI
Feature: Verify reading a feed from the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed

    @smoke @test
    Scenario Outline: Read a feed from the Feeds List
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I read the first feed's url & title
        And I click on the first feed from list
        Then Verify the feed title and url are displayed
        Examples:
            | pagename |
            | All      |

    @smoke @test
    Scenario Outline: Read a feed detail from the Feeds detail list
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I click on the first feed from list
        And I read the first feeds' detail
        And I click on the first feed detail
        Then Verify the feeds detail is displayed
        Examples:
            | pagename |
            | All      |