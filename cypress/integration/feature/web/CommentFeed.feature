@UI
Feature: Verify adding comment to a feed in the application

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the login link
        And I login with credentials
            | username  | password |
            | autotest1 | pass1234 |
        Then Verify logout link is displayed

    @smoke @test
    Scenario Outline: Add a comment to a feed's detail
        When I click on '<pagename>' feed page link
        And There are feeds available
        And I click on the first feed from list
        And I read the first feeds' detail
        And I read the first feed's comment count
        And I click on the first feed detail
        Then Verify the feeds detail is displayed
        And I add a new comment
        And I navigate back
        Then First feed's comment count should be incremented 
        Examples:
            | pagename |
            | All      |