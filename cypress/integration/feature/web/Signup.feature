@UI
Feature: Verify the signup functionality of the application
    As a user I want to sign up as a new user

    Background: Launch the app and open the login page
        Given I visit the application
        When I click on the Signup button

    @smoke @test
    Scenario Outline: Signup with a valid credentials and login with the same credentials
        And I Signup with '<username>' and '<password>' 
        Then Verify logout link is displayed
        And Logout of the application
        When I click on the login link
        And I login with the same credentials
        Then Verify logout link is displayed
        And Logout of the application
        Examples:
            | username          | password |
            | autotest          | pass1234 |
            | autotest@.u+s-e_r | pass1234 |


    Scenario Outline: Signup with invalid credentials
        And I Signup with invalid '<username>' and '<password>'
        Then Verify logout link is not displayed
        And Verify error '<error>' is displayed
        Examples:
            | username             | password | error                                                                                          |
            | autotest123          | 12345678 | This password is entirely numeric.                                                             |
            | autotest123          | pass12   | This password is too short. It must contain at least 8 characters.                             |
            | autotest@.u+s-e_r#!% | pass     | Enter a valid username. This value may contain only letters, numbers and @/./+/-/_ characters. |
            | autotest             | pass1234 | A user with that username already exists.                                                      |


