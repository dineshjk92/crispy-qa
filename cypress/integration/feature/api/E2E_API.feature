@API @E2E

Feature: Verify end-to-end workflow of the given application

    As a user, I want to verify the end-to-end business flow in the application
    Background: Authenticate the app
        Given I request login page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200
        And The response should contain "href=\"/accounts/logout/\""

    Scenario Outline: Verify happy path flow
        When I request add new feed page
        And I add a new feed '<url>' via api
        Then The response code should be 200
        And The response should contain '<url>'
        And The feed should be stored in db
        When I fetch the feed's id from db
        Then The response should contain redirections with params "/feeds/" and "@feed_id"
        And I fetch the feed's first entry id from db for the '<url>' with "non-zero" items
        When I make a call to the feed's first entry
        Then The response code should be 200
        When I add a comment to the feed entry
        Then The response code should be 200
        And The response should contain the username and comment message
        When I make a call to the feeds detail
        Then The response code should be 200
        When I read the last checked time from the response
        And I make a call to update the feed
        Then The response code should be 200
        When I read the update message from the response
        Then The response should contain the update message
        And Last checked time from the response should not be the same when the update alert is 'Found new updates. Enjoy!'
        When I make a call to the feeds detail
        Then The response code should be 200
        When I fetch the '<url>' feeds bookmark status for the loggedin user
        And I make a call to "add" bookmark to the feed if not added already
        Then The response code should be 200
        When The response should "not contain" "glyphicon-heart-empty"
        And Logout via api
        Examples:
            | url                                        |
            | https://rss.app/feeds/DexrFv7yOSqTeRUg.xml |

    Scenario Outline: Verify unhappy path flow - feed with no items
        When I request add new feed page
        And I add a new feed '<url>' via api
        Then The response code should be 500
        And The response should contain '<url>'
        And The feed should be stored in db
        When I fetch the feed's id from db
        And I fetch the feed's first entry id from db for the '<url>' with "zero" items
        When I make a call to the feed's first entry
        Then The response code should be 500
        And Logout via api
        Examples:
            | url                                                                                          |
            | https://gitlab.com/dineshjk92/crispy-qa/-/raw/master/cypress/fixtures/rssfeedwithnoitems.xml |