@UI
Feature: Verify the signup functionality via API

    Background: Launch the app
        Given I request signup page

    Scenario Outline: Signup with a valid credentials via api
        When I Signup with auto-generated '<username>' and '<password>'
        Then The response code should be 200
        And The response should contain "href=\"/accounts/logout/\""
        And The response should contain "Great success! Enjoy :)"
        And The userdetails should be stored in db
        And Logout via api
        When I request login page
        And I login with the same credentials via api
        Then The response code should be 200
        And The response should contain "href=\"/accounts/logout/\""
        And Logout via api
        Examples:
            | username | password |
            | autotest | pass1234 |

    Scenario: Sing up via api with an existing user from db
        And I read a username from db
        When I Signup with the existing username
        Then The response code should be 200
        And The response should contain "A user with that username already exists."

    Scenario: Login via api redirection to All Feeds page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200
        And The response should contain redirections "/feeds/"
        And The response should contain "href=\"/accounts/logout/\""
        And Logout via api