@UI
Feature: Verify the login functionality via API

    Background: Launch the app
        Given I request login page

    Scenario: Login with a valid credentials via api
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200
        And The response should contain "href=\"/accounts/logout/\""
        And Logout via api

    Scenario: Login with invalid credentials via api
        And I login via api
            | username  | password |
            | autotest1 | pass     |
        Then The response code should be 200
        And The response should contain "Please enter a correct username and password. Note that both fields may be case-sensitive."

    Scenario: Login via api redirection to All Feeds page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200
        And The response should contain redirections "/feeds/"
        And The response should contain "href=\"/accounts/logout/\""
        And Logout via api