@UI

Feature: Verify the adding new feed via API

    Background: Authenticate the app
        Given I request login page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200

    Scenario Outline: Add a valid new feed url
        When I request add new feed page
        And I add a new feed '<url>' via api
        Then The response code should be 200
        And The response should contain '<url>'
        And The feed should be stored in db
        When I fetch the feed's id from db
        Then The response should contain redirections with params "/feeds/" and "@feed_id"
        And Logout via api
        Examples:
            | url                                                                                          |
            | https://rss.app/feeds/DexrFv7yOSqTeRUg.xml                                                   |
            | https://gitlab.com/dineshjk92/crispy-qa/-/raw/master/cypress/fixtures/rssfeedwithnoitems.xml |

    Scenario: Add an existing new feed url
        And I read an existing feed url from db
        When I request add new feed page
        And I add the existing feed url
        Then The response code should be 200
        And The response should contain 'Feed with this Feed URL already exists.'
        And Logout via api

    Scenario Outline: Add an invalid feed url
        When I request add new feed page
        And I add a new feed '<url>' via api
        Then The response code should be '<statuscode>'
        And The response should contain '<url>'
        And The response should contain '<error>'
        And The feed should not be stored in db
        And Logout via api
        Examples:
            | url                                                                                    | error                         | statuscode |
            | https://gitlab.com/dineshjk92/crispy-qa/-/raw/master/cypress/fixtures/emptyrssfeed.xml | NoReverseMatch at /feeds/new/ | 500        |
            | This is an invalid feed url                                                            | Enter a valid URL.            | 200        |
            | feeds.feedburner.com/tweakers/mixed                                                    | Enter a valid URL.            | 200        |
            | https://en.wikipedia.org/wiki/RSS                                                      | NoReverseMatch at /feeds/new/ | 500        |

    Scenario Outline: Date added time for new feed url
        When I request add new feed page
        And I read the current date and time
        And I add a new feed '<url>' via api
        Then The response code should be 200
        And The response should contain 'Check for updates'
        And The feed should be stored in db
        When I fetch the feed's id from db
        Then The response should contain redirections with params "/feeds/" and "@feed_id"
        When I make a call to the feeds detail
        And The Date added in the response should match with the timestamp
        And Logout via api
        Examples:
            | url                                        |
            | https://rss.app/feeds/f8IwulX1xaiNdvKM.xml |
