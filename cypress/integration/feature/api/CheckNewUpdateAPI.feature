@UI

Feature: Verify checking update for feeds via API

    Background: Authenticate the app
        Given I request login page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200

    Scenario Outline: Last checked time for a feed after new update
        And I fetch the feed's id from db for the '<url>'
        When I make a call to the feeds detail
        Then The response code should be 200
        When I read the last checked time from the response
        And I make a call to update the feed
        Then The response code should be 200
        When I read the update message from the response
        Then The response should contain the update message
        And Last checked time from the response should not be the same when the update alert is 'Found new updates. Enjoy!'
        And Logout via api

        Examples:
            | url                                                                                          |
            | http://www.nu.nl/rss/Algemeen                                                                |
            | https://gitlab.com/dineshjk92/crispy-qa/-/raw/master/cypress/fixtures/rssfeedwithnoitems.xml |

    Scenario Outline: Entries count for a feed after new update
        And I fetch the feed's id from db for the '<url>'
        And I fetch the feed's entry count from db for the '<url>'
        When I make a call to the feeds detail
        Then The response code should be 200
        And I make a call to update the feed
        Then The response code should be 200
        When I read the update message from the response
        Then The response should contain the update message
        When I make a call to all feeds list
        Then Entries count in the response for the '<url>' should not be the same when the update alert is 'Found new updates. Enjoy!'
        And Logout via api

        Examples:
            | url                                        |
            | https://rss.app/feeds/DexrFv7yOSqTeRUg.xml |
