@UI

Feature: Verify adding comments for feeds via API

    Background: Authenticate the app
        Given I request login page
        When I login via api
            | username  | password |
            | autotest1 | pass1234 |
        Then The response code should be 200

    Scenario Outline: Add a feed to bookmarked
        And I fetch the feed's id from db for the '<url>'
        When I make a call to the feeds detail
        Then The response code should be 200
        When I fetch the '<url>' feeds bookmark status for the loggedin user
        And I make a call to "add" bookmark to the feed if not added already
        Then The response code should be 200
        When The response should "not contain" "glyphicon-heart-empty"  
        And Logout via api

        Examples:
            | url                           |
            | http://www.nu.nl/rss/Algemeen |

    Scenario Outline: REmove a feed from bookmarked
        And I fetch the feed's id from db for the '<url>'
        When I make a call to the feeds detail
        Then The response code should be 200
        When I fetch the '<url>' feeds bookmark status for the loggedin user
        And I make a call to "remove" bookmark to the feed if not added already
        Then The response code should be 200
        And The response should "contain" "glyphicon-heart-empty"  
        And Logout via api

        Examples:
            | url                           |
            | http://www.nu.nl/rss/Algemeen |