const logoutLink = "a[href$='/logout/']";
const feedsTableHeaders = 'table  > thead > tr > th'
const allFeedsLink = "a[href$='/feeds/']"
const myFeedsLink = "a[href$='/feeds/my/']"
const bookmarkedLink = "a[href$='/feeds/bookmarked/']"
const newFeedButton = "a[href$='/feeds/new/']"
const feedsLinksTable = 'table.table > tbody > tr > td:nth-child(1) > a'
const feedsUrlsTable = 'table.table > tbody > tr > td:nth-child(3)'
const feedsEntriesCount = 'table.table > tbody > tr > td:nth-child(4)'

class CommonPage {
  getLogoutLink() {
    return cy.get(logoutLink);
  }

  logout() {
    cy.get(logoutLink).click()
  }

  getTableHeader() {
    const headers = [] 
    cy.get(feedsTableHeaders).each((ele) => {
      headers.push(ele.text())
    })
    return headers
  }

  gotoAllFeedsPage() {
    cy.get(allFeedsLink).click()
  }

  gotoMyFeedsPage() {
    cy.get(myFeedsLink).click()
  }

  gotoBookmarkedFeedsPage() {
    cy.get(bookmarkedLink).click()
  }

  gotoNewFeedsPage() {
    cy.get(newFeedButton).click()
  }

  getFirstFeedsTitle() {
    return cy.get(feedsLinksTable).first()
  }

  getFirstFeedsUrl() {
    return cy.get(feedsUrlsTable).first()
  }

  clickOnFirstFeed() {
    cy.get(feedsLinksTable).first().click()
  }

  getFeedsEntryCount() {
    return cy.get(feedsEntriesCount).first()
  }

}
export default CommonPage;
