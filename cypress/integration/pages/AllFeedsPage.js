
import CommonPage from "./CommonPage";

const header = 'header > h1'

class AllFeedsPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

}
export default AllFeedsPage;
