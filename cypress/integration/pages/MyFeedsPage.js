
import CommonPage from "./CommonPage";

const header = 'header > h1'

class MyFeedsPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

}
export default MyFeedsPage;
