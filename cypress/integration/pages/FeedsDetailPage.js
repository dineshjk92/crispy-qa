
import CommonPage from "./CommonPage";

const header = 'header > h1'
const commentTextBox = '#id_content'
const submit = '#submit-id-submit'

class FeedsDetailPage extends CommonPage {

    getHeaderText() {
        return cy.get(header).text()
    }

    addComment(comment) {
        cy.get(commentTextBox).invoke('show').type(comment)
        cy.get(submit).click()
    }
}
export default FeedsDetailPage;